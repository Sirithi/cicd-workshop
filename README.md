# CICD Workshop

Use this repo to gain some valuable knowledge about the inner workings of Gitlab pipelines and deployment of applications to for example AWS.

## Getting started

It is recommended to start with bronze and work your way up the medals as time passes. Do whatever tickles your fancy.

- 🥇 Deploy a clean Laravel install or your own project on to AWS Elastic Beanstalk 
- 🥈 Deploy one of the games in this repo to AWS S3 (optionally using Cloudfront) 
- 🥉 Build the code of either the Chess or Tic-Tac-Toe in a Gitlab pipeline 
